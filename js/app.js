// /js/app.js
document.addEventListener('DOMContentLoaded', () => {
    const movieDetailsContainer = document.getElementById('movieDetails');
    const searchForm = document.getElementById('searchForm');
  
    searchForm.addEventListener('submit', function (event) {
      event.preventDefault();
  
      const movieTitle = document.getElementById('movieTitle').value;
  
      if (movieTitle.trim() === '') {
        alert('Por favor, ingresa el título de la película.');
        return;
      }
  
      // Cambia la URL de la API a HTTPS
      fetch(`https://www.omdbapi.com/?t=${encodeURIComponent(movieTitle)}&plot=full&apikey=30063268`)
        .then(response => response.json())
        .then(data => {
          if (data.Error) {
            alert('Película no encontrada. Inténtalo de nuevo con otro título.');
          } else {
            const normalizedTitle = data.Title.toLowerCase();
            if (normalizedTitle === movieTitle.toLowerCase()) {
              const movieDetailsHTML = `
                <h2>${data.Title}</h2>
                <p><strong>Año de estreno:</strong> ${data.Year}</p>
                <p><strong>Actores:</strong> ${data.Actors}</p>
                <p><strong>Reseña:</strong> ${data.Plot}</p>
                <img src="${data.Poster}" alt="Poster de la película">
              `;
              movieDetailsContainer.innerHTML = movieDetailsHTML;
            } else {
              alert('El título ingresado no coincide exactamente con ninguna película.');
            }
          }
        })
        .catch(error => console.error("Error al consumir la API:", error));
    });
  });
  